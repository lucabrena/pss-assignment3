package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.junit.Test;

import daos.EntityManagerSingleton;
import entities.Group;
import entities.Status;
import entities.User;
import services.GroupService;
import services.StatusService;
import services.UserService;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserTest {

	private User u1 = new User("Luca", "Brena", "luca.brena@unimib.it", "password1");
	private User u2 = new User("Mattia", "Artifoni", "mattia.artifoni@unimib.it", "password2");
	private User u3 =  new User("Davide", "Samotti",  "davide.samotti@unimib.it", "password3");
	private Group g1 = new Group("Gruppo1", "public");
	private Group g2 = new Group("Gruppo2", "public");	
	private Status s1 =  new Status("Titolo", "Testo", "12/01/2019");
	private Status s2 =  new Status("Titolo2", "Testo2", "21/01/2019");
	
	private UserService us = new UserService();
	private StatusService ss = new StatusService();
	private GroupService gs = new GroupService();
	
	@Before
	public void setUpDb() {
		final EntityManager entityManager = Persistence.createEntityManagerFactory("demo").createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("DELETE IGNORE FROM friend_of").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM User_group").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM Status").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM Groups").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM User").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
	}
	
	

	@Test
	public void testSaveUser() {
		us.save(u1);
		assertEquals(us.get(u1.getId()), u1);
	}
	

	@Test
	public void testGetUser() {
		us.save(u1);
		us.save(u2);
		us.save(u3);
		
		assertEquals(us.get(u1.getId()), u1);
		
		List<User> persistedUsers = us.getAll();
		List<User> expectedPersistedUsers = new ArrayList<User>();
		expectedPersistedUsers.add(u1);
		expectedPersistedUsers.add(u2);
		expectedPersistedUsers.add(u3);
		
		assertEquals(persistedUsers, expectedPersistedUsers);
	}
	

	@Test
	public void testUpdateUser() {
		us.save(u1);
		
		
		assertEquals(u1.getName(), "Luca");		
		u1.setName("Gianfranco");
		us.update(u1);
		assertEquals(u1.getName(), "Gianfranco");
		
	}
	

	@Test
	public void testDeleteUser() {
		us.save(u1);
		us.save(u2);
		
		// Un utente non pu� eliminare un altro utente, per farlo deve essere admin
		us.delete(u1, u2);
		us.delete(u1, u1);
		
		assertTrue(us.getAll().contains(u2));
		assertFalse(us.getAll().contains(u1));
		
	}
	
	@Test
	public void testGetRoleUser() {
		us.save(u1);
		assertEquals(us.getUserRole(u1), "user");
	}
	
	
	@Test
	public void testFriend(){		
		us.save(u1);
		us.save(u2);
		us.save(u3);

		// u1 e u2 diventano amici. 
		// u1 e u3 diventano amici.
		us.addFriend(u1, u2);
		us.addFriend(u1, u3);
		
		List<User> friends = us.getAllFriends(u1);

		assertTrue(friends.contains(u2));
		assertTrue(friends.contains(u3));
		
		//u1 e u2 non sono pi� amici. 
		us.deleteFriend(u1, u2);
		
		//u2 contiua a esistere
		assertTrue(us.getAll().contains(u2));
		// ma viene elimina la relazione con u1
		assertFalse(u1.getMyFriends().contains(u2));
		
		List<User> nonFriends = us.getNonFriends(u1);
		
		assertTrue(nonFriends.contains(u2));
		assertFalse(u2.getMyFriends().contains(u1));
		assertTrue(u1.getMyFriends().contains(u3));
		
		// Elimino un utente che ha degli amici. Elimina la relazione ma 
		// u2 e u3 esistono ancora
		us.addFriend(u1, u2);
		us.delete(u1, u1);
		
		assertFalse(us.getAll().contains(u1));
		assertTrue(us.getAll().contains(u2));
		assertTrue(us.getAll().contains(u3));
		assertFalse(u2.getMyFriends().contains(u1));
		assertFalse(u3.getMyFriends().contains(u1));
	}
	
	@Test
	public void testUserGroup(){		
		us.save(u1);
		us.save(u2);
		us.save(u3);		
		gs.save(g1);
		gs.save(g2);
		
		us.addUserToGroup(g1, u1);
		us.addUserToGroup(g1, u2);
		us.addUserToGroup(g1, u3);
		us.addUserToGroup(g2, u1);
		us.addUserToGroup(g2, u2);
		
		assertTrue(u1.getJoinedGroups().contains(g1));
		assertTrue(u1.getJoinedGroups().contains(g2));
		assertTrue(g1.getMembers().contains(u1));
		assertTrue(g1.getMembers().contains(u2));
		assertTrue(g1.getMembers().contains(u3));
		assertTrue(g2.getMembers().contains(u1));
		assertTrue(g2.getMembers().contains(u2));
		
		gs.deleteUserFromGroup(g2, u1);
		assertFalse(g2.getMembers().contains(u1));
		
		gs.deleteUserFromGroup(g2, u2);
		
		//Eliminando tutti i membri di un gruppo anche il gruppo viene eliminato
		
		assertFalse(gs.getAll().contains(g2));		
		assertFalse(u2.getJoinedGroups().contains(g2));
		
		
		
	}
	
	
	@Test
	public void testUserStatus(){		
		us.save(u1);
		ss.save(s1);
		ss.save(s2);
		
		// u1 pubblica gli status s1 e s2
		us.addUserStatus(u1, s1);
		us.addUserStatus(u1, s2);
		
		assertTrue(u1.getStatus().contains(s1));
		assertEquals(s1.getAuthor(), u1);
		
		// Se elimino un utente anche i suoi status vengono eliminati
		us.delete(u1, u1);
		assertFalse(ss.getAll().contains(s1));
		assertFalse(ss.getAll().contains(s2));

	}
	
}
