package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.junit.Test;

import daos.EntityManagerSingleton;
import entities.Admin;
import entities.Group;
import entities.Status;
import entities.User;
import services.GroupService;
import services.StatusService;
import services.UserService;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdminTest {

	private User u1 = new User("Luca", "Brena", "luca.brena@unimib.it", "password1");
	private User u2 = new User("Mattia", "Artifoni", "mattia.artifoni@unimib.it", "password2");
	private User u3 =  new User("Davide", "Samotti",  "davide.samotti@unimib.it", "password3");
	private Group g1 = new Group("Gruppo1", "public");
	private Group g2 = new Group("Gruppo2", "public");	
	private Status s1 =  new Status("Titolo", "Testo", "12/01/2019");
	private Status s2 =  new Status("Titolo2", "Testo2", "21/01/2019");
	
	private Admin a1 = new Admin("LucaAdmin", "BrenaAdmin", "luca.brenaAdmin@unimib.it", "password1");
	private Admin a2 = new Admin("MattiaAdmin", "ArtifoniAdmin", "mattia.artifoniAdmin@unimib.it", "password2");
	private Admin a3 = new Admin("DavideAdmin", "SamottiAdmin",  "davide.samottiAdmin@unimib.it", "password3");
	
	private UserService us = new UserService();
	private StatusService ss = new StatusService();
	private GroupService gs = new GroupService();
	
	@Before
	public void setUpDb() {
		final EntityManager entityManager = Persistence.createEntityManagerFactory("demo").createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("DELETE IGNORE FROM friend_of").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM User_group").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM Status").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM Groups").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM User").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();
	}
	
	@Test
	public void testSaveAdmin() {
		us.save(a1);
		assertEquals(us.get(a1.getId()), a1);
	}
	

	@Test
	public void testGetAdmin() {
		us.save(a1);
		us.save(a2);
		us.save(a3);
		
		assertEquals(us.get(a1.getId()), a1);
		
		List<User> persistedUsers = us.getAll();
		List<User> expectedPersistedUsers = new ArrayList<User>();
		expectedPersistedUsers.add(a1);
		expectedPersistedUsers.add(a2);
		expectedPersistedUsers.add(a3);
		
		assertEquals(persistedUsers, expectedPersistedUsers);
	}
	

	@Test
	public void testUpdateAdmin() {
		us.save(a1);
		
		
		assertEquals(a1.getName(), "LucaAdmin");		
		a1.setName("GianfrancoAdmin");
		us.update(a1);
		assertEquals(a1.getName(), "GianfrancoAdmin");
		
	}
	

	@Test
	public void testDeleteAdmin() {
		us.save(a1);
		us.save(a2);
		us.save(u1);
		
		// Un admin pu� eliminare un qualsiasi altro utente e pu� eliminare un'altro admin
		us.delete(a1, u1);
		us.delete(a1, a2);
		us.delete(a1, a1);
		
		
		assertFalse(us.getAll().contains(a2));
		assertFalse(us.getAll().contains(a1));
		assertFalse(us.getAll().contains(u1));
		
		
	}
	
	@Test
	public void testGetRoleAdmin() {
		us.save(a1);
		assertEquals(us.getUserRole(a1), "admin");
	}
	
	
	@Test
	public void testFriend(){		
		us.save(a1);
		us.save(a2);
		us.save(u3);

		// a1 e a2 diventano amici. 
		// a1 e a3 diventano amici.
		us.addFriend(a1, a2);
		us.addFriend(a1, u3);
		
		List<User> friends = us.getAllFriends(a1);

		assertTrue(friends.contains(a2));
		assertTrue(friends.contains(u3));
		
		//a1 e a2 non sono pi� amici. 
		us.deleteFriend(a1, a2);
		
		//a2 contiua a esistere
		assertTrue(us.getAll().contains(a2));
		// ma viene elimina la relazione con a1
		assertFalse(a1.getMyFriends().contains(a2));
		
		List<User> nonFriends = us.getNonFriends(a1);
		
		assertTrue(nonFriends.contains(a2));
		assertFalse(a2.getMyFriends().contains(a1));
		assertTrue(a1.getMyFriends().contains(u3));
		
		// Elimino un utente che ha degli amici. Elimina la relazione ma 
		// a2 e a3 esistono ancora
		us.addFriend(a1, a2);
		us.delete(a1, a1);
		
		assertFalse(us.getAll().contains(a1));
		assertTrue(us.getAll().contains(a2));
		assertTrue(us.getAll().contains(u3));
		assertFalse(a2.getMyFriends().contains(a1));
		assertFalse(u3.getMyFriends().contains(a1));
	}
	
	@Test
	public void testAdminGroup(){		
		us.save(a1);
		us.save(a2);
		us.save(u3);		
		gs.save(g1);
		gs.save(g2);
		
		us.addUserToGroup(g1, a1);
		us.addUserToGroup(g1, a2);
		us.addUserToGroup(g1, u3);
		us.addUserToGroup(g2, a1);
		us.addUserToGroup(g2, a2);
		
		assertTrue(a1.getJoinedGroups().contains(g1));
		assertTrue(a1.getJoinedGroups().contains(g2));
		assertTrue(g1.getMembers().contains(a1));
		assertTrue(g1.getMembers().contains(a2));
		assertTrue(g1.getMembers().contains(u3));
		assertTrue(g2.getMembers().contains(a1));
		assertTrue(g2.getMembers().contains(a2));
		
		gs.deleteUserFromGroup(g2, a1);
		assertFalse(g2.getMembers().contains(a1));
		
		gs.deleteUserFromGroup(g2, a2);
		
		//Eliminando tutti i membri di un gruppo anche il gruppo viene eliminato
		
		assertFalse(gs.getAll().contains(g2));		
		assertFalse(a2.getJoinedGroups().contains(g2));
		
		
		
	}
	
	
	@Test
	public void testAdminStatus(){		
		us.save(a1);
		ss.save(s1);
		ss.save(s2);
		
		// a1 pubblica gli status s1 e s2
		us.addUserStatus(a1, s1);
		us.addUserStatus(a1, s2);
		
		assertTrue(a1.getStatus().contains(s1));
		assertEquals(s1.getAuthor(), a1);
		
		// Se elimino un utente anche i suoi status vengono eliminati
		us.delete(a1, a1);
		assertFalse(ss.getAll().contains(s1));
		assertFalse(ss.getAll().contains(s2));

	}
	
}
