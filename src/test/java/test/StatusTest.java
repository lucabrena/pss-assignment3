package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.junit.Test;

import daos.EntityManagerSingleton;
import entities.Group;
import entities.Status;
import entities.User;
import services.GroupService;
import services.StatusService;
import services.UserService;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StatusTest {

	private User u1 = new User("Luca", "Brena", "luca.brena@unimib.it", "password1");
	private User u2 = new User("Mattia", "Artifoni", "mattia.artifoni@unimib.it", "password2");
	private User u3 =  new User("Davide", "Samotti",  "davide.samotti@unimib.it", "password3");
	private Group g1 = new Group("Gruppo1", "public");
	private Group g2 = new Group("Gruppo2", "public");	
	private Status s1 =  new Status("Titolo", "Testo", "12/01/2019");
	private Status s2 =  new Status("Titolo2", "Testo2", "21/01/2019");
	
	private UserService us = new UserService();
	private StatusService ss = new StatusService();
	private GroupService gs = new GroupService();
	
	@Before
	public void setUpDb() {
		final EntityManager entityManager = Persistence.createEntityManagerFactory("demo").createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("DELETE IGNORE FROM friend_of").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM User_group").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM Status").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM Groups").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM User").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();

	}
	

	@Test
	public void testSaveStatus() {
		ss.save(s1);
		assertNotNull(ss.get(s1.getId()));
	}
	

	@Test
	public void testGetStatus() {
		ss.save(s1);
		ss.save(s2);
		
		assertEquals(ss.get(s1.getId()), s1);
		
		List<Status> statusList = new ArrayList<Status>();	
		statusList.add(s1);
		statusList.add(s2);

		assertEquals(ss.getAll(), statusList);
	}
	

	@Test
	public void testUpdateStatus() {
		ss.save(s1);
		ss.save(s2);
		
		assertEquals(s1.getTitle(), "Titolo");		
		s1.setTitle("TitoloModificato");
		ss.update(s1);
		assertEquals(s1.getTitle(), "TitoloModificato");
		
	}
	

	@Test
	public void testDeleteStatus() {
		ss.save(s1);
		ss.delete(s1);
		assertFalse(ss.getAll().contains(s1));
	}
	
	
	@Test
	public void testStatusUser(){
		us.save(u1);
		ss.save(s1);
		ss.save(s2);

	
		
		// u1 pubblica gli status s1 e s2
		us.addUserStatus(u1, s1);
		us.addUserStatus(u1, s2);
		
		// Eliminando gli status l'utente non viene eliminato
		ss.delete(s1);
		ss.delete(s2);
		
		assertFalse(ss.getAll().contains(s1));
		assertFalse(ss.getAll().contains(s2));
		assertNotNull(us.get(u1.getId()));


	}

}
