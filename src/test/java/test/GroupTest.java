package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.junit.Test;

import daos.EntityManagerSingleton;
import entities.Group;
import entities.Status;
import entities.User;
import services.GroupService;
import services.StatusService;
import services.UserService;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GroupTest {

	private User u1 = new User("Luca", "Brena", "luca.brena@unimib.it", "password1");
	private User u2 = new User("Mattia", "Artifoni", "mattia.artifoni@unimib.it", "password2");
	private User u3 =  new User("Davide", "Samotti",  "davide.samotti@unimib.it", "password3");
	private Group g1 = new Group("Gruppo1", "public");
	private Group g2 = new Group("Gruppo2", "public");	
	private Status s1 =  new Status("Titolo", "Testo", "12/01/2019");
	private Status s2 =  new Status("Titolo2", "Testo2", "21/01/2019");
	
	private UserService us = new UserService();
	private StatusService ss = new StatusService();
	private GroupService gs = new GroupService();
	
	@Before
	public void setUpDb() {
		final EntityManager entityManager = Persistence.createEntityManagerFactory("demo").createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("DELETE IGNORE FROM friend_of").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM User_group").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM Status").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM Groups").executeUpdate();
        entityManager.createNativeQuery("DELETE IGNORE FROM User").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.close();

	}
	
	
	
	@Test
	public void testSaveGroup() {
		gs.save(g1);
		assertNotNull(gs.get(g1.getId()));
	}
	

	@Test
	public void testGetGroup() {
		gs.save(g1);
		gs.save(g2);
		
		assertEquals(gs.get(g1.getId()), g1);
		
		List<Group> groupList = new ArrayList<Group>();	
		groupList.add(g1);
		groupList.add(g2);

		assertEquals(gs.getAll(), groupList);
	}
	

	@Test
	public void testUpdateGroup() {
		gs.save(g1);
		gs.save(g2);
		
		assertEquals(g1.getName(), "Gruppo1");		
		g1.setName("Gruppo1Modificato");
		gs.update(g1);
		assertEquals(g1.getName(), "Gruppo1Modificato");
		
	}
	

	@Test
	public void testDeleteGroup() {
		gs.save(g1);
		gs.delete(g1);
		assertFalse(gs.getAll().contains(g1));
	}
	
	
	
	@Test
	public void testGroupUser(){
		us.save(u1);
		us.save(u2);
		us.save(u3);		
		gs.save(g1);
		gs.save(g2);
		
		us.addUserToGroup(g1, u1);
		us.addUserToGroup(g1, u2);
		us.addUserToGroup(g1, u3);
		
		gs.delete(g1);
		
		// Elimino le relazioni tra gruppo e utenti ma gli utenti non vengono eliminati
		assertNotNull(us.get(u1.getId()));
		assertNotNull(us.get(u2.getId()));
		assertNotNull(us.get(u3.getId()));
		
		
	}

}
