package daos;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import entities.Status;
import entities.User;

import java.util.*;
import java.util.function.Consumer;

public class StatusDao extends AbstractDao<Status>{
	
	
	@Override
	public void save(Status status) {
		super.save(status);
	}
	
	@Override
	public Status get(Class<Status> statusClass, int id) {
		return super.get(statusClass, id);
	}
	
	
	@Override
	public List<Status> getAll(Class<Status> target){
		return super.getAll(target);
	}
	
	
	public List<Status> getAll(Class<Status> status, User user){
		List<Status> statusVector = getEntityManager().createQuery("SELECT s FROM Status s WHERE s.author = :u", status).setParameter("u", user).getResultList();
		ArrayList<Status> statusArray = new ArrayList<Status>(statusVector);
		return statusArray;
	}
	
	@Override
	public void delete(Status status) {
		getEntityManager().remove(status);
	}
	
	@Override
	public void update(Status status) {
		getEntityManager().merge(status);
	}
}
