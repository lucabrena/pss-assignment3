package daos;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import entities.Admin;
import entities.User;

import java.util.*;
import java.util.function.Consumer;

public class AdminDao extends AbstractDao<Admin>{
	
	private UserDao userdao;
	
	public AdminDao(){
		this.userdao = new UserDao();
	}
	
	@Override
	public void save(Admin admin) {
		userdao.save(admin);
	}
	
	@Override
	public Admin get(Class<Admin> adminClass, int id) {
		return super.get(adminClass, id);
	}
	
	@Override
	public List<Admin> getAll(Class<Admin> target){
		return super.getAll(target);
	}
	

	public void delete(User user) {
		userdao.delete(user);
	}
}
