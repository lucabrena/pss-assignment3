
package daos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import entities.User;

public abstract class AbstractDao<T> implements Dao<T>{
	
	public void save(T t) {
		getEntityManager().persist(t);
		
	}
	
	public T get(Class<T> target, int id) {
		T t = getEntityManager().find(target, id);
		return t;
	}
	

	public List<T> getAll(Class<T> target){
		List<T> t = getEntityManager().createQuery("SELECT u FROM " + target.getSimpleName() + " u", target).getResultList();
		// Cast vector -> array 
		ArrayList<T> l = new ArrayList<T>(t);
		return l;
	}
	
	public void delete(T t) {
		getEntityManager().remove(t);
	}
	
	public void update(T t) {
		getEntityManager().merge(t);
	}
    public void beginTransaction() {
    	getEntityManager().getTransaction().begin();
    }

    public void commitTransaction() {
    	getEntityManager().flush();
    	getEntityManager().getTransaction().commit();
    }	
    
    public EntityManager getEntityManager() {
    	return EntityManagerSingleton.getEntityManager();
    }
    
    public void rollbackTransaction() {
    	getEntityManager().getTransaction().rollback();
    }


}
