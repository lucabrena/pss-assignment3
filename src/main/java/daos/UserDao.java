package daos;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import entities.Status;
import entities.User;

import java.util.*;
import java.util.function.Consumer;


public class UserDao extends AbstractDao<User>{
	
	
	@Override
	public void save(User user) {
		super.save(user);
	}
	
	@Override
	public User get(Class<User> userClass, int id) {
		return super.get(userClass, id);
	}
	
	@Override
	public List<User> getAll(Class<User> target){
		return super.getAll(target);
	}
	
	@Override
	public void delete(User user) {
		if(!(user.getMyFriends().isEmpty())) {
			getEntityManager().createNativeQuery(
					"DELETE FROM friend_of "
					+ "WHERE  friend1 =" + user.getId() + " OR friend2 = "+  user.getId())
			.executeUpdate();
		}			
		super.delete(user);
	}
	
	@Override
	public void update(User user) {
		super.update(user);
	}
	
	
	public List<User> getAllFriends(User user) {
		// List<User> friendsVector =
		// EntityManagerSingleton.getEntityManager().createQuery("SELECT f FROM
		// friend_of f WHERE f.friend1 = :u", User.class).setParameter("u",
		// user).getResultList();
		List<Integer> friendsVector = getEntityManager().createNativeQuery("SELECT friend2 FROM friend_of  WHERE friend1 = " + user.getId()).getResultList();
		ArrayList<User> friendsArray = new ArrayList<User>();
		for (Integer i : friendsVector) {			
			friendsArray.add(this.get(User.class, i));
		}
		return friendsArray;
	}

	public List<User> getNonFriends(User user) {
		List<User> friendsVector = this.getAllFriends(user);
		List<User> allUsers = this.getAll(User.class);
		allUsers.removeAll(friendsVector);
		allUsers.remove(user);
		ArrayList<User> friendsArray = new ArrayList<User>(allUsers);
		return friendsArray;
	}	
	
	public String getUserRole(User user) {
		List<String> role = getEntityManager().createNativeQuery("SELECT role  FROM " +
				 "User  WHERE id = " + user.getId()).getResultList();
		return role.get(0);
	}
	
	
}
