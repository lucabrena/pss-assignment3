package daos;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import entities.Group;

import java.util.*;
import java.util.function.Consumer;

public class GroupDao extends AbstractDao<Group>{
	
	
	@Override
	public void save(Group group) {
		super.save(group);
	}
	
	@Override
	public Group get(Class<Group> groupClass, int id) {
		return super.get(groupClass, id);
	}
	
	@Override
	public List<Group> getAll(Class<Group> target){
		return super.getAll(target);
	}
	
	@Override
	public void delete(Group group) {
		super.delete(group);
	}
	
	@Override
	public void update(Group group) {
		super.update(group);
	}
}
