
package entities;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="User")
@DiscriminatorValue("admin")
public class Admin extends User{	
	public Admin(){
		super();
	}
	
	public Admin(String name, String surname, String mail, String password) {
		super(name, surname, mail, password);
	}		
}
