package entities;

import static org.junit.Assert.assertTrue;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

@Entity
@Table(name="Groups")
public class Group implements Serializable{
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name="name")
	private String name;
	@Column(name="visibility")
	private String visibility;
	
	@ManyToMany(mappedBy = "joinedGroups")
	private Set<User> members;
	
	
	public Group() {
		super();
	}

	public Group(String name, String visibility) {
		super();
		this.name = name;
		this.visibility = visibility;
		this.members = new HashSet<User>();
		//Aggiungere come primo membro il creatore del gruppo...
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public Set<User> getMembers() {
		return members;
	}

	public void setMembers(Set<User> members) {
		this.members = members;
	}

	public int getId() {
		return id;
	}
	
	public void addUser(User user) {
		members.add(user);
	}
	
	public void deleteUserFromGroup(User user) {
		this.getMembers().remove(user);
		user.getJoinedGroups().remove(this);
	}

	@Override
	public String toString() {
		String membersString = "";
		for(User user : this.members) {
			membersString += user.toString();
		}
		return "Group [name=" + name + ", visibility=" + visibility + ", members=" + membersString + "]";
	}
	
	
	
	
}
