package entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "Status")
public class Status implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "title")
	private String title;
	@Column(name = "text",
			length = 512)
	private String text;
	@Column(name = "date")
	private String date;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "author")
	public User author;

	public User getAuthor() {
		return author;
	}

	@Override
	public String toString() {
		return "Status [id=" + id + ", title=" + title + ", text=" + text + ", date=" + date + ", author=" + author
				+ "]";
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public Status(String title, String text, String date) {
		super();
		this.title = title;
		this.text = text;
		this.date = date;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Status() {
		super();
	}

}
