package entities;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="User")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "role")
@DiscriminatorValue("user")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name="name")
	private String name;
	@Column(name="surname")
	private String surname;
	@Column(name="mail", unique = true)
	private String mail;
	@Column(name="password")
	private String password;
	
	@JoinTable(name = "friend_of", schema = "maven", joinColumns = {
			 @JoinColumn(name = "friend1", nullable = false)},inverseJoinColumns = {
			 @JoinColumn(name = "friend2", nullable = false)})
	@ManyToMany(cascade = CascadeType.PERSIST)
	private Set<User> myFriends;
	
	
	@JoinTable(name = "User_group", schema = "maven", joinColumns = {
			 @JoinColumn(name = "user_id", nullable = false)}, inverseJoinColumns = {
			 @JoinColumn(name = "group_id", nullable = false)})
	@ManyToMany(cascade = CascadeType.PERSIST)
	private Set<Group> joinedGroups;
	
	
	 @OneToMany(
		        mappedBy = "author",
		        cascade = CascadeType.ALL,
		        orphanRemoval = true
		    )
		   
	private Set<Status> status;
	
	
	public User(){
		super();
	}
	
	public User(String name, String surname, String mail, String password) {
		this.name = name;
		this.surname = surname;
		this.mail = mail;
		this.password = password;
		this.myFriends = new HashSet<User>();
		this.joinedGroups = new HashSet<Group>();
		this.status = new HashSet<Status>();
	}	
	
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", surname=" + surname + ", mail=" + mail + ", password="
				+ password + "]";
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	

	public Set<User> getMyFriends() {
		return myFriends;
	}

	public void setMyFriends(Set<User> myFriends) {
		this.myFriends = myFriends;
	}

	public Set<Group> getJoinedGroups() {
		return joinedGroups;
	}

	public void setJoinedGroups(Set<Group> joinedGroups) {
		this.joinedGroups = joinedGroups;
	}

	public Set<Status> getStatus() {
		return status;
	}

	public void setStatus(Set<Status> status) {
		this.status = status;
	}
	
	
	public void addFriend(User u1) {
		myFriends.add(u1);
		u1.getMyFriends().add(this);

	}
	
	public void deleteFriend(User u1) {
		myFriends.remove(u1);
		u1.getMyFriends().remove(this);
	}
	
	public void addGroup(Group g1) {
		joinedGroups.add(g1);
	}
	
	public void addStatus(Status s1) {
		status.add(s1);
		s1.setAuthor(this);
	}
}
