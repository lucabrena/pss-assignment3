package services;


import java.util.List;

import daos.StatusDao;
import entities.Status;
import entities.User;


public class StatusService {
	StatusDao statusdao;
	public StatusService() {
		statusdao = new StatusDao();
	}
	
	public void save(Status status) {
		statusdao.beginTransaction();
		statusdao.save(status);
		statusdao.commitTransaction();
		
	}
	
	public Status get(int id) {
		statusdao.beginTransaction();
		Status status = statusdao.get(Status.class, id);
		statusdao.commitTransaction();
		return status;
	}
	
	public List<Status> getAll() {
		statusdao.beginTransaction();
		List<Status> status = statusdao.getAll(Status.class);
		statusdao.commitTransaction();
		return status;
	}
	
	public List<Status> getAll(User user) {
		statusdao.beginTransaction();
		List<Status> status = statusdao.getAll(Status.class, user);
		statusdao.commitTransaction();
		return status;
	}
	
	public void delete(Status status) {
		statusdao.beginTransaction();
		User author = status.getAuthor();
		if(author != null)
			author.getStatus().remove(status);
		statusdao.delete(status);
		statusdao.commitTransaction();
	}
	
	public void update(Status status) {
		statusdao.beginTransaction();
		statusdao.update(status);
		statusdao.commitTransaction();
	}
}