package services;


import java.util.Iterator;
import java.util.List;
import java.util.Set;

import daos.EntityManagerSingleton;
import daos.GroupDao;
import entities.Group;
import entities.User;


public class GroupService {
	GroupDao groupdao;
	
	public GroupService() {
		groupdao = new GroupDao();
	}
	
	public void save(Group group) {
		groupdao.beginTransaction();
		groupdao.save(group);
		groupdao.commitTransaction();
		
	}
	
	public Group get(int id) {
		groupdao.beginTransaction();
		Group u = groupdao.get(Group.class, id);
		groupdao.commitTransaction();
		return u;
	}
	
	public List<Group> getAll() {
		groupdao.beginTransaction();
		List<Group> u = groupdao.getAll(Group.class);
		groupdao.commitTransaction();
		return u;
	}
	

	public void delete(Group group) {
		groupdao.beginTransaction();
		Set<User> members = group.getMembers();
		Iterator<User> iteratorMembers = members.iterator();
		 while(iteratorMembers.hasNext()) {
		        User userMember = iteratorMembers.next();
		        userMember.getJoinedGroups().remove(group);		        
		}
		groupdao.update(group); 
		groupdao.delete(group);
		groupdao.commitTransaction();
	}
	
	public void update(Group group) {
		groupdao.beginTransaction();
		groupdao.update(group);
		groupdao.commitTransaction();
	}
	
	public void deleteUserFromGroup(Group group, User user) {
		groupdao.beginTransaction();
		//Controllo che l'utente sia l'unico componente del gruppo, se cos fosse
		//Elimino il gruppo
		group.deleteUserFromGroup(user);
		Set<User> members = group.getMembers();
		if (members.isEmpty())
			groupdao.delete(group);
		groupdao.commitTransaction();

	}
}