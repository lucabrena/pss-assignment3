package services;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import daos.UserDao;
import entities.Group;
import entities.Status;
import entities.User;

public class UserService {
	UserDao userdao;

	public UserService() {
		userdao = new UserDao();
	}

	public void save(User user) {
		userdao.beginTransaction();
		userdao.save(user);
		userdao.commitTransaction();

	}

	public User get(int id) {
		userdao.beginTransaction();
		User u = userdao.get(User.class, id);
		userdao.commitTransaction();
		return u;
	}

	public List<User> getAll() {
		userdao.beginTransaction();
		List<User> u = userdao.getAll(User.class);
		userdao.commitTransaction();
		return u;
	}

	public List<User> getAllFriends(User user) {
		userdao.beginTransaction();
		List<User> userFriends = userdao.getAllFriends(user);
		userdao.commitTransaction();
		return userFriends;
	}

	public List<User> getNonFriends(User user) {
		userdao.beginTransaction();
		List<User> userNonFriends = userdao.getNonFriends(user);
		userdao.commitTransaction();
		return userNonFriends;
	}

	public void addFriend(User friendAdder, User friendAdded) {
		userdao.beginTransaction();
		friendAdder.addFriend(friendAdded);
		userdao.update(friendAdder);
		userdao.commitTransaction();
	}

	public void deleteFriend(User friendAdder, User friendAdded) {
		userdao.beginTransaction();
		friendAdder.deleteFriend(friendAdded);
		userdao.update(friendAdder);
		userdao.commitTransaction();
	}


	public void delete(User requester, User target) {
        
        //Solo un admin può eliminare un altro utente
		if (userdao.getUserRole(requester).equals("admin") || (userdao.getUserRole(target).equals("user")
				&& userdao.getUserRole(requester).equals("user") && requester.getId() == target.getId())) {

			userdao.beginTransaction();
			// Se l'utente da eliminare ha amici, prima di eliminarlo, lo elimino dalla lista amici dei suoi amici
			if (!(target.getMyFriends().isEmpty())) {
				Set<User> friends = target.getMyFriends();
				Iterator<User> iteratorFriends = friends.iterator();
				while (iteratorFriends.hasNext()) {
					User userFriend = iteratorFriends.next();
					userFriend.getMyFriends().remove(target);
				}
			}
			// Come prima, ma se il target ha joinato dei gruppo
			if (!(target.getJoinedGroups().isEmpty())) {
				Set<Group> groups = target.getJoinedGroups();
				Iterator<Group> iteratorGroups = groups.iterator();
				while (iteratorGroups.hasNext()) {
					Group userGroup = iteratorGroups.next();
					userGroup.getMembers().remove(target);
				}
			}			
			userdao.update(target);
			userdao.delete(target);
			userdao.commitTransaction();
		}

	}

	public void update(User user) {
		userdao.beginTransaction();
		userdao.update(user);
		userdao.commitTransaction();
	}

	public Set<Group> getJoinedGroups(int idUser) {
		userdao.beginTransaction();
		User user = userdao.get(User.class, idUser);
		Set<Group> joinedGroups = user.getJoinedGroups();
		userdao.commitTransaction();
		return joinedGroups;
	}

	public String getUserRole(User user) {
		userdao.beginTransaction();
		String role = userdao.getUserRole(user);
		userdao.commitTransaction();
		return role;
	}

	public void addUserToGroup(Group group, User user) {
		userdao.beginTransaction();
		Set<User> members = group.getMembers();
		if (members.contains(user))
			userdao.rollbackTransaction();
		else {
			group.addUser(user);
			user.addGroup(group);
		}
		userdao.update(user);
		userdao.commitTransaction();

	}
	
	

	public void addUserStatus(User user, Status status) {
		userdao.beginTransaction();
		user.addStatus(status);
		userdao.update(user);
		userdao.commitTransaction();

	}

}