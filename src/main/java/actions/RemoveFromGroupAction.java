package actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import entities.Group;
import entities.Status;
import entities.User;

import com.opensymphony.xwork2.ActionSupport;

import services.GroupService;
import services.StatusService;
import services.UserService;

public class RemoveFromGroupAction extends ActionSupport {

	private static final long serialVersionUID = 7299264265184515893L;

	private String mail;
	private String groupid;
	private String userid;

	@Override
	public String execute() {

		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		mail = (String) session.getAttribute("loginedmail");
		if (mail != null) {
			UserService userService = new UserService();
			User user = userService.get(Integer.parseInt(userid));
			GroupService groupService = new GroupService();
			Group group=groupService.get(Integer.parseInt(groupid));
			List<Group> groupList=new ArrayList<Group>(userService.getJoinedGroups(Integer.parseInt((String) session.getAttribute("loginedid"))));
			if (groupList.indexOf(group) != -1) {
				groupService.deleteUserFromGroup(group, user);
			} else {
				return "error";
			}
			return "success";
		} else {
			return "error";
		}

	}

	public String getMail() {
		return mail;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

}