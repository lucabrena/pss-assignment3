package actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import entities.Group;
import entities.Status;
import entities.User;

import com.opensymphony.xwork2.ActionSupport;

import services.GroupService;
import services.StatusService;
import services.UserService;

public class AddMeToGroupAction extends ActionSupport {

	private static final long serialVersionUID = 7299264265184515893L;

	private String mail;
	private String groupid;

	@Override
	public String execute() {

		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		mail = (String) session.getAttribute("loginedmail");
		if (mail != null) {
			UserService userService = new UserService();
			User loggedUser = userService.get(Integer.parseInt((String) session.getAttribute("loginedid")));
			GroupService groupService = new GroupService();
			Group group = groupService.get(Integer.parseInt(groupid));
			if (group.getVisibility().equals("public")) {
				userService.addUserToGroup(group, loggedUser);
			} else {
				return "error";
			}
			return "success";
		} else {
			return "error";
		}

	}

	public String getMail() {
		return mail;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

}