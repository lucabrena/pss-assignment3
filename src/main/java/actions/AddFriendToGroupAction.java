package actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import entities.Group;
import entities.Status;
import entities.User;

import com.opensymphony.xwork2.ActionSupport;

import services.GroupService;
import services.StatusService;
import services.UserService;

public class AddFriendToGroupAction extends ActionSupport {

	private static final long serialVersionUID = 7299264265184515893L;

	private String mail;
	private String friendid;
	private String groupid;

	@Override
	public String execute() {

		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		mail = (String) session.getAttribute("loginedmail");
		if (mail != null) {
			UserService userService = new UserService();
			User friendUser = userService.get(Integer.parseInt(friendid));
			GroupService groupService = new GroupService();
			Group group = groupService.get(Integer.parseInt(groupid));
			User loggedUser = userService.get(Integer.parseInt((String) session.getAttribute("loginedid")));
			List<User> friendsList = userService.getAllFriends(loggedUser);
			List<Group> groupsList = new ArrayList<Group>(userService.getJoinedGroups(Integer.parseInt((String) session.getAttribute("loginedid"))));
			if (friendsList.indexOf(friendUser) != -1 && groupsList.indexOf(group) != -1) {
				userService.addUserToGroup(group, friendUser);
			} else {
				return "error";
			}
			// used to pass group id directly to another action
			session.setAttribute("groupid", groupid);
			return "success";
		} else {
			return "error";
		}

	}

	public String getMail() {
		return mail;
	}

	public void setFriendid(String friendid) {
		this.friendid = friendid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

}