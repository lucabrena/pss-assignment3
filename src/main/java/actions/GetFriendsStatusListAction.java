package actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import entities.Status;
import entities.User;

import com.opensymphony.xwork2.ActionSupport;

import services.StatusService;
import services.UserService;

public class GetFriendsStatusListAction extends ActionSupport {

	private static final long serialVersionUID = 7299264265184515893L;

	private String mail;
	private String list;
	private String adminpermissions;

	@Override
	public String execute() {

		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		mail=(String) session.getAttribute("loginedmail");
	    if (mail!=null) {
			UserService userService = new UserService();
			User loggedUser = userService.get(Integer.parseInt((String)session.getAttribute("loginedid")));
			Set<User> userFriendsSet=loggedUser.getMyFriends();
			int f=0;
			list="";
			for(User friend: userFriendsSet) {
	            if(f==0) {f=1;list+="<p>My Friends:</p>";}
				list+="<p>"+friend.getName()+" "+friend.getSurname()+"<form name='deletefriend' action='/pss-assignment3/deletefriend.action' method='post'>"
					+ "<input type='hidden' name='friendid' value='" + friend.getId() + "'/>"
					+ "<input type='submit' name='method:execute' value='DeleteFriend' /></form></p>";
			}
			if(f==1) {list+="<br />";}
			StatusService statusService = new StatusService();
			List<Status> statusList=new ArrayList<Status>();
			f=0;
			for(User friend: userFriendsSet) {
				statusList=statusService.getAll(friend);
				for(Status status: statusList) {
					if(f==0) {f=1;list+="<p>Friends Status:</p>";}
				    list+="<textarea rows='5' cols='50' style='overflow:scroll; resize:none;'>"+status.getText()+"</textarea>"+
				          "<p> | "+status.getAuthor().getName()+" "+status.getAuthor().getSurname()+" | "+status.getDate()+"</p><br />";
				}
			}
			f=0;
			List<Status> userStatusList = statusService.getAll(loggedUser);
			for(Status status: userStatusList) {
				if(f==0) {f=1;list+="<p>My Status:</p>";}
			    list+="<textarea rows='5' cols='50' style='overflow:scroll; resize:none;'>"+status.getText()+"</textarea>"+
			          "<p> | "+status.getAuthor().getName()+" "+status.getAuthor().getSurname()+" | "+status.getDate()+"</p><br />";
			}
			adminpermissions = "";
			if (userService.getUserRole(loggedUser).equals("admin")) {
				adminpermissions += "<form name='deleteuserpage' action='/pss-assignment3/deleteuserpage.action' method='post'>"
						          + "<input type='submit' name='method:execute' value='DeleteAnUser' /></form>";
			}
			return "success";
		} else {
			return "error";
		}

	}

	public String getMail() {
		return mail;
	}

	public String getList() {
		return list;
	}

	public String getAdminpermissions() {
		return adminpermissions;
	}

}