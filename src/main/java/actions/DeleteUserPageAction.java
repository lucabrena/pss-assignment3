package actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import entities.Status;
import entities.User;

import com.opensymphony.xwork2.ActionSupport;

import services.StatusService;
import services.UserService;

public class DeleteUserPageAction extends ActionSupport {

	private static final long serialVersionUID = 7299264265184515893L;

	private String mail;
	private String list;

	@Override
	public String execute() {

		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		mail=(String) session.getAttribute("loginedmail");
	    if (mail!=null) {
	    	UserService userService = new UserService();
			User loggedUser = userService.get(Integer.parseInt((String)session.getAttribute("loginedid")));
			List<User> userList=userService.getAll();
			userList.remove(loggedUser);
			list="";
			for(User user: userList) {
			    list+="<p>"+user.getName()+" "+user.getSurname()+" | "+userService.getUserRole(user)+" | "+
			          "<form name='deleteuser' action='/pss-assignment3/deleteuser.action' method='post'>"+
				      "<input type='hidden' name='userid' value='" + user.getId() + "'/>"+
					  "<input type='submit' name='method:execute' value='DeleteUser'/></form></p>";
			}
			return "success";
		} else {
			return "error";
		}

	}

	public String getMail() {
		return mail;
	}
	
	public String getList() {
		return list;
	}

}