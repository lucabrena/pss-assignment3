package actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import entities.Group;
import entities.Status;
import entities.User;

import com.opensymphony.xwork2.ActionSupport;

import services.GroupService;
import services.StatusService;
import services.UserService;

public class GroupsAction extends ActionSupport {

	private static final long serialVersionUID = 7299264265184515893L;

	private String mail;
	private String list;

	@Override
	public String execute() {

		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		mail = (String) session.getAttribute("loginedmail");
		if (mail != null) {
			UserService userService = new UserService();
			GroupService groupService = new GroupService();

			List<Group> groupList = new ArrayList<Group>(groupService.getAll());
			List<Group> publicGroupList = new ArrayList<Group>();
			for (Group group : groupList) {
				if (group.getVisibility().equals("public")) {
					publicGroupList.add(group);
				}
			}
			List<Group> joinedGroupList = new ArrayList<Group>(userService.getJoinedGroups(Integer.parseInt((String) session.getAttribute("loginedid"))));
			publicGroupList.removeAll(joinedGroupList);

			list = "";
			int f = 0;
			for (Group group : publicGroupList) {
				if (f == 0) {
					f = 1;
					list += "<p>Public Groups:</p>";
				}
				list += "<p>" + group.getName() + " | " + group.getVisibility() + " | "
						+ "<form name='addmetogroup' action='/pss-assignment3/addmetogroup.action' method='post'>"
						+ "<input type='hidden' name='groupid' value='" + group.getId() + "'/>"
						+ "<input type='submit' name='method:execute' value='JoinGroup' /></form></p>" + "<br />";
			}

			f = 0;
			for (Group group : joinedGroupList) {
				if (f == 0) {
					f = 1;
					list += "<p>Joined Groups:</p>";
				}
				list += "<p>" + group.getName() + " | " + group.getVisibility() + " | "
						+ "<form name='viewgrouppage' action='/pss-assignment3/viewgrouppage.action' method='post'>"
						+ "<input type='hidden' name='groupid' value='" + group.getId() + "'/>"
						+ "<input type='submit' name='method:execute' value='ViewGroup' /></form>"
						+ "<form name='exitfromgroup' action='/pss-assignment3/exitfromgroup.action' method='post'>"
						+ "<input type='hidden' name='groupid' value='" + group.getId() + "'/>"
						+ "<input type='submit' name='method:execute' value='ExitFromGroup' /></form>"
						+ "<form name='deletegroup' action='/pss-assignment3/deletegroup.action' method='post'>"
						+ "<input type='hidden' name='groupid' value='" + group.getId() + "'/>"
						+ "<input type='submit' name='method:execute' value='DeleteGroup' /></form></p>" + "<br />";
			}
			return "success";
		} else {
			return "error";
		}

	}

	public String getMail() {
		return mail;
	}

	public String getList() {
		return list;
	}

}