package actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import entities.Status;
import entities.User;

import com.opensymphony.xwork2.ActionSupport;

import services.StatusService;
import services.UserService;

public class DeleteFriendAction extends ActionSupport {

	private static final long serialVersionUID = 7299264265184515893L;

	private String mail;
	private String friendid;

	@Override
	public String execute() {

		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		mail = (String) session.getAttribute("loginedmail");
		if (mail != null) {
			UserService userService = new UserService();
			User loggedUser = userService.get(Integer.parseInt((String) session.getAttribute("loginedid")));
			User friend = userService.get(Integer.parseInt(friendid));
			List<User> friendsList = userService.getAllFriends(loggedUser);
			if (friendsList.indexOf(friend) != -1) {
				userService.deleteFriend(loggedUser, friend);
			} else {
				return "error";
			}
			return "success";
		} else {
			return "error";
		}

	}

	public String getMail() {
		return mail;
	}

	public void setFriendid(String friendid) {
		this.friendid = friendid;
	}

}