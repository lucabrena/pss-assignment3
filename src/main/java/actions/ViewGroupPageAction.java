package actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import entities.Group;
import entities.Status;
import entities.User;

import com.opensymphony.xwork2.ActionSupport;

import services.GroupService;
import services.StatusService;
import services.UserService;

public class ViewGroupPageAction extends ActionSupport {

	private static final long serialVersionUID = 7299264265184515893L;

	private String mail;
	private String groupid;
	private String list;
	private String groupname;

	@Override
	public String execute() {

		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		mail = (String) session.getAttribute("loginedmail");
		if (mail != null) {
			UserService userService = new UserService();
			User loggedUser = userService.get(Integer.parseInt((String) session.getAttribute("loginedid")));
			GroupService groupService = new GroupService();
			Group group;
			if (groupid != null) {
				group = groupService.get(Integer.parseInt(groupid));

				List<Group> groupList = new ArrayList<Group>(
						userService.getJoinedGroups(Integer.parseInt((String) session.getAttribute("loginedid"))));
				if (groupList.indexOf(group) == -1) {
					return "erorr";
				}
			} else {
				groupid=(String) session.getAttribute("groupid");
				session.removeAttribute("groupid");
				group = groupService.get(Integer.parseInt(groupid));
			}
			groupname = group.getName();
			List<User> joinedUsersList = new ArrayList<User>(group.getMembers());
			System.out.println(joinedUsersList);
			List<User> usersList = new ArrayList<User>(userService.getAllFriends(loggedUser));
			usersList.removeAll(joinedUsersList);
			list = "";
			for (User user : joinedUsersList) {
				list += "<p>" + user.getName() + " " + user.getSurname();
				if (user.getId() != loggedUser.getId()) {
					list += "<form name='removefromgroup' action='/pss-assignment3/removefromgroup.action' method='post'>"
							+ "<input type='hidden' name='groupid' value='" + group.getId() + "'/>"
							+ "<input type='hidden' name='userid' value='" + user.getId() + "'/>"
							+ "<input type='submit' name='method:execute' value='RemoveFromGroup' /></form>";
				}
				list += "</p>";
			}
			if (!usersList.isEmpty()) {
				list += "<p>Add a new user:</p>";
			}
			for (User user : usersList) {
				list += "<p>" + user.getName() + " " + user.getSurname() + " | "
						+ "<form name='addfriendtogroup' action='/pss-assignment3/addfriendtogroup.action' method='post'>"
						+ "<input type='hidden' name='groupid' value='" + groupid + "'/>"
						+ "<input type='hidden' name='friendid' value='" + user.getId() + "'/>"
						+ "<input type='submit' name='method:execute' value='AddFriendToGroup' /></form></p>";
			}
			return "success";
		} else {
			return "error";
		}

	}

	public String getMail() {
		return mail;
	}

	public String getList() {
		return list;
	}

	public String getGroupname() {
		return groupname;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

}