package actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import entities.Status;
import entities.User;

import com.opensymphony.xwork2.ActionSupport;

import services.StatusService;
import services.UserService;

public class UpdateStatusAction extends ActionSupport {

	private static final long serialVersionUID = 7299264265184515893L;

	private String mail;
	private String statusid;
	private String text;

	@Override
	public String execute() {

		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		mail = (String) session.getAttribute("loginedmail");
		if (mail != null) {
			StatusService statusService = new StatusService();
			Status status = statusService.get(Integer.parseInt(statusid));
			status.setText(text);

			UserService userService = new UserService();
			User loggedUser = userService.get(Integer.parseInt((String) session.getAttribute("loginedid")));
			List<Status> statusList = statusService.getAll(loggedUser);
			if (statusList.indexOf(status) != -1) {
				statusService.update(status);
			} else {
				return "error";
			}
			return "success";
		} else {
			return "error";
		}

	}

	public String getMail() {
		return mail;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setStatusid(String statusid) {
		this.statusid = statusid;
	}

}