package actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import entities.Status;
import entities.User;
import entities.Group;

import com.opensymphony.xwork2.ActionSupport;

import services.GroupService;
import services.StatusService;
import services.UserService;

public class CreateGroupAction extends ActionSupport {

	private static final long serialVersionUID = 7299264265184515893L;

	private String mail;
	private String name;
	private String visibility;

	@Override
	public String execute() {

		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		mail=(String) session.getAttribute("loginedmail");
	    if (mail!=null) {
			UserService userService = new UserService();
			User loggedUser = userService.get(Integer.parseInt((String)session.getAttribute("loginedid")));
			Group group = new Group(name, visibility);
			userService.addUserToGroup(group, loggedUser);
			return "success";
		} else {
			return "error";
		}

	}

	public String getMail() {
		return mail;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

}