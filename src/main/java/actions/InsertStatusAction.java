package actions;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import entities.Status;
import entities.User;

import com.opensymphony.xwork2.ActionSupport;

import services.StatusService;
import services.UserService;

public class InsertStatusAction extends ActionSupport {

	private static final long serialVersionUID = 7299264265184515893L;

	private String mail;
	private String title;
	private String text;
	private String date;

	@Override
	public String execute() {

		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session = request.getSession();
		mail=(String) session.getAttribute("loginedmail");
	    if (mail!=null) {
			StatusService statusService = new StatusService();
			UserService userService = new UserService();
			User loggedUser = userService.get(Integer.parseInt((String)session.getAttribute("loginedid")));
			Status status = new Status(title, text, date);
			userService.addUserStatus(loggedUser, status);			
			return "success";
		} else {
			return "error";
		}

	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setDate(String date) {
		this.date = date;
	}

}