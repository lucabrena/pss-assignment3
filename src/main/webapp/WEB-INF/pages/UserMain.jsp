<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Main page</title>
</head>
 
<body>
<h2>WELCOME <s:property value="mail" /></h2>
<s:property escape="false" value="list" />
<s:form action="viewstatuspage" method="post" namespace="/">
  <s:submit method="execute" key="View Status" value="ManageMyStatus" />
</s:form>
<s:form action="insertstatuspage" method="post" namespace="/">
  <s:submit method="execute" key="Insert Status" value="InsertStatus" />
</s:form>
<s:form action="addfriendpage" method="post" namespace="/">
  <s:submit method="execute" key="Add Friend" value="AddFriend" />
</s:form>
<s:form action="groupspage" method="post" namespace="/">
  <s:submit method="execute" key="Groups" value="Groups" />
</s:form>
<s:form action="deleteaccount" method="post" namespace="/">
  <s:submit method="execute" key="Delete Account" value="DeleteAccount" />
</s:form>
<s:property escape="false" value="adminpermissions" />
<s:form action="loginpage" method="post" namespace="/">
  <s:submit method="execute" key="Login Page" value="TakeMeToLoginPage" />
</s:form>
</body>
</html>