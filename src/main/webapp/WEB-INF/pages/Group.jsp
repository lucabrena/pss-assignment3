<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title><s:property escape="false" value="groupname" /> page</title>
</head>
 
<body>
<h2>WELCOME <s:property value="mail" /></h2>
<h3><s:property value="groupname" /></h3>
<p>Joined users:</p>
<s:property escape="false" value="list" />

<s:form action="groupspage" method="post" namespace="/">
  <s:submit method="execute" key="Groups" value="BackToGroups" />
</s:form>
</body>
</html>