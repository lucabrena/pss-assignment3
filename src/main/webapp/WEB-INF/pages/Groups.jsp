<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Groups page</title>
</head>
 
<body>
<h2>WELCOME <s:property value="mail" /></h2>
<s:property escape="false" value="list" />
<s:actionerror />
<s:form action="creategroup" method="post" namespace="/">
  <s:textfield label="Name" name="name" key="Name" size="50" value="MagistraleInformatica" />
  <s:select label="Visibility" headerKey="-1" list="#{'public':'public', 'private':'private'}" name="visibility" value="#{'public'}" />
  <s:submit method="execute" key="Submit" value="CreateGroup" />
</s:form>

<s:form action="mainpage" method="post" namespace="/">
  <s:submit method="execute" key="Main Page" value="TakeMeToMainPage" />
</s:form>
</body>
</html>