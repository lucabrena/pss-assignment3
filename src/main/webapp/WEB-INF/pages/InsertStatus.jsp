<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>Insert Status page</title>
</head>
 
<body>
<h2>WELCOME <s:property value="mail" /></h2>
<s:actionerror />
<s:form action="insertstatus" method="post" namespace="/">
  <s:textfield name="title" key="Title" size="50" value="Come praparare una tisana allo zenzero" />
  <s:textarea name="text" key="Text" cols="50" rows="10" cssStyle="overflow:scroll; resize:none;" value="La tisana allo zenzero si prepara semplicemente sbucciando una radice di zenzero fresco, tagliandola in piccoli pezzetti, per due persone, ne bastano 5/6 pezzetti, essendo molto forte e leggermente piccante. Per preparare una perfetta tisana, basta  far bollire l’acqua e lo zenzero per circa quattro minuti, si spegne la fiamma e si aggiunge il succo di limone spremuto fresco, si cola il tutto  e si mette un cucchiaino di miele, d’acacia è ideale, o a seconda dei gusti." />
  <s:textfield name="date" key="Date" size="50" value="10/gen/2019" />
  <s:submit method="execute" key="Submit" value="SubmitStatus" />
</s:form>

<s:form action="mainpage" method="post" namespace="/">
  <s:submit method="execute" key="Main Page" value="TakeMeToMainPage" />
</s:form>
</body>
</html>